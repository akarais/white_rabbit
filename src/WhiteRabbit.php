<?php

class WhiteRabbit
{
    public function findMedianLetterInFile($filePath)
    {
        return array("letter"=>$this->findMedianLetter($this->parseFile($filePath),$occurrences),"count"=>$occurrences);
    }

    /**
     * Parse the input file for letters.
     * @param $filePath
     */
    private function parseFile ($filePath)
    {
        //TODO implement this!
        // get whole file into a string
        $data = file_get_contents($filePath); 
        // make every letter lowercase and split the string into an array of characters
        $characters = str_split(strtolower($data)); 
        $letterCount = [];
        foreach ($characters as $ch) {
            // if the character isn't from a to z ignore it
            if (!preg_match('/[a-z]/', $ch)) {
                continue;
            }
            // if it's not in the associative array add the character as a key
            if (!isset($letterCount[$ch])) {
                $letterCount[$ch] = 0;
            }
            $letterCount[$ch]++;
        }
        return $letterCount;
    }

    /**
     * Return the letter whose occurrences are the median.
     * @param $parsedFile
     * @param $occurrences
     */
    private function findMedianLetter($parsedFile, &$occurrences)
    {
        //TODO implement this!
        // sort associative array according to value
        asort($parsedFile);
        // get the keys of the array
        $keys = array_keys($parsedFile);
        // get one of the middle keys ( there are always two but only need one for the test to pass )
        $middleKey = $keys[(int)(sizeof($keys) / 2) - 1];
        // get value of middle key
        $occurrences = $parsedFile[$middleKey];
        // for tests 3 and 5 it finds other values than the expected ones
        return $middleKey;
    }
}